//
//  Gift.h
//  gift
//
//  Created by Kyle Schwarzkopf on 6/8/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event;

@interface Gift : NSManagedObject

@property (nonatomic, retain) NSString * giftIdea;
@property (nonatomic, retain) Event *event;
@property (nonatomic, retain) NSManagedObject *giftIdeas;

@end
