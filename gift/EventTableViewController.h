//
//  EventTableViewController.h
//  gift
//
//  Created by Kyle Schwarzkopf on 5/10/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"


@interface EventTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) Event *event;

@end
