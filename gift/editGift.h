//
//  editGift.h
//  gift
//
//  Created by Kyle Schwarzkopf on 6/3/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "Gift.h"
#import "Event.h"

@interface editGift : UIViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) Event *ListGift;
@property (nonatomic, strong) Gift *addGift;
@property (weak, nonatomic) IBOutlet UITextView *editTextView;

@end
