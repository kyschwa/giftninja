//
//  CoreViewController.m
//  gift
//
//  Created by Kyle Schwarzkopf on 5/10/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "CoreViewController.h"
#import "AppDelegate.h"

@interface CoreViewController ()

@end

@implementation CoreViewController

-(NSManagedObjectContext *)managedObjectContext {
    
    return [(AppDelegate *)[[UIApplication sharedApplication]delegate]managedObjectContext];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)cancelAndDismiss {
    
    [self.managedObjectContext rollback];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)saveAndDismiss {
    
    NSError *error = nil;
    if ([self.managedObjectContext hasChanges]) {
        if (![self.managedObjectContext save:&error]) { //save failed
            NSLog(@"Save Failed: %@", [error localizedDescription]);
        } else {
            NSLog(@"Save Succeeded");
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end

