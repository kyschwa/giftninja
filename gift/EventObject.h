//
//  EventObject.h
//  gift
//
//  Created by Kyle Schwarzkopf on 5/11/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventObject.h"

@interface EventObject : NSObject


@property (strong, nonatomic) NSDate *dateOfBirth;


- (id)initWithName:(NSString *)name dateOfBirth:(NSDate *)dateOfBirth placeOfBirth:(NSString *)placeOfBirth;

@end
