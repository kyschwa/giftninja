//
//  AddEventViewController.h
//  gift
//
//  Created by Kyle Schwarzkopf on 5/10/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreViewController.h"
#import "Event.h"

@class EventObject;

@interface AddEventViewController : CoreViewController <UITableViewDataSource, UITableViewDelegate, NSObject>

@property (nonatomic, strong)Event *addEvent;

@property (weak, nonatomic) IBOutlet UITextField *addName;
@property (weak, nonatomic) IBOutlet UITextField *addEventName;

- (IBAction)save:(UIBarButtonItem *)sender;
- (IBAction)cancel:(UIBarButtonItem *)sender;

@end
