//
//  Event.m
//  gift
//
//  Created by Kyle Schwarzkopf on 6/8/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "Event.h"
#import "Gift.h"


@implementation Event

@dynamic eventDate;
@dynamic eventDateString;
@dynamic eventName;
@dynamic name;
@dynamic reminderDate;
@dynamic reminderDateString;
@dynamic gifts;
@dynamic addGift2;


@end
