//
//  editGift.m
//  gift
//
//  Created by Kyle Schwarzkopf on 6/3/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "editGift.h"
#import "EditEventTableViewController.h"
#import "AppDelegate.h"

@interface editGift ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation editGift
@synthesize ListGift, addGift;

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


#pragma load/fetch data
-(NSManagedObjectContext *)managedObjectsContext {
    
    return [(AppDelegate*)[[UIApplication sharedApplication] delegate]managedObjectContext];
    
}

#pragma save and cancel
-(IBAction)save:(UIBarButtonItem *)sender {
    
    
    ListGift.reminderDateString = _editTextView.text;
    
    
    NSError *error = nil;
    if ([self.managedObjectContext hasChanges]) {
        if (![self.managedObjectContext save:&error]) { //save failed
            NSLog(@"Save Failed: %@", [error localizedDescription]);
        } else {
            NSLog(@"Save Succeeded");
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)cancel:(UIBarButtonItem *)sender {
    
    [self.managedObjectContext rollback];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

@end
