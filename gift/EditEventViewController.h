//
//  EditEventViewController.h
//  gift
//
//  Created by Kyle Schwarzkopf on 5/10/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "CoreViewController.h"

@interface EditEventViewController : CoreViewController

@property (strong, nonatomic) Event *editEvent;

@property (weak, nonatomic) IBOutlet UITextField *editName;
@property (weak, nonatomic) IBOutlet UITextField *editEventName;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editSaveButton;


- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)editSave:(UIBarButtonItem *)sender;



@end
