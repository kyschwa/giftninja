//
//  giftIdea.m
//  gift
//
//  Created by Kyle Schwarzkopf on 6/3/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "giftIdea.h"
#import "AppDelegate.h"
#import "AddEventTableViewController.h"
#import "giftIdea.h"

@interface giftIdea ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation giftIdea
@synthesize giftList;

- (IBAction)done:(UIBarButtonItem *)sender {
    
    [self.managedObjectContext rollback];
    
//    NSError *error = nil;
//    if ([self.managedObjectContext hasChanges]) {
//        if (![self.managedObjectContext save:&error]) { //save failed
//            NSLog(@"Save Failed: %@", [error localizedDescription]);
//        } else {
//            NSLog(@"Save Succeeded");
//        }
//    }
//    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma generic code
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSError *error = nil;
    
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"Error! %@", error);
        abort();
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [self.tableView reloadData];
    
}

#pragma load/fetch data
-(NSManagedObjectContext *)managedObjectsContext {
    
    return [(AppDelegate*)[[UIApplication sharedApplication] delegate]managedObjectContext];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UITableViewCell *)sender {
    
    if ([[segue identifier] isEqualToString:@"viewGift"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        
        AddEventTableViewController*addTableEventController = (AddEventTableViewController *)navigationController.topViewController;
        
        Event *addEvent = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:[self managedObjectsContext]];
        
        addTableEventController.addEvent = addEvent;
    }if ([[segue identifier] isEqualToString:@"doneSegue"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        
        AddEventTableViewController*addTableEventController = (AddEventTableViewController *)navigationController.topViewController;
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        Event *editEvent = (Event*)[self.fetchedResultsController objectAtIndexPath:indexPath];
        
        addTableEventController.addEvent= editEvent;
    }
}

#pragma tableview datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    Event *event = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
//    cell.textLabel.text = event.giftIdea;
    
    return cell;
}

#pragma edit cells
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSManagedObjectContext *context = [self managedObjectsContext];
        
        Event *eventToDelete = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        [context deleteObject:eventToDelete];
        
        NSError *error = nil;
        
        if (![context save:&error]) {
            NSLog(@"Error! %@", error);
        }
    }
}

#pragma fetched results contoller section - important code snipets

-(NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSManagedObjectContext *context = [ self managedObjectsContext];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"giftIdeas" ascending:YES];
    
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    fetchRequest.sortDescriptors = sortDescriptors;
    
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    
   _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

#pragma fetched results contoller delegates

-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView beginUpdates];
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.tableView endUpdates];
    
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView; //creating a temporary placeholder
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate: {
            Event *changeEvent = [self.fetchedResultsController objectAtIndexPath:indexPath];
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//            cell.textLabel.text = changeEvent.giftIdeas;
        }
            break;
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
    
}


@end
