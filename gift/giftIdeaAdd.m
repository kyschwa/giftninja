//
//  giftIdeaAdd.m
//  gift
//
//  Created by Kyle Schwarzkopf on 6/3/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "giftIdeaAdd.h"
#import "AppDelegate.h"
#import "AddEventTableViewController.h"

@interface giftIdeaAdd ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;



@end

@implementation giftIdeaAdd
@synthesize giftList, textView;


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


#pragma load/fetch data
-(NSManagedObjectContext *)managedObjectsContext {
    
    return [(AppDelegate*)[[UIApplication sharedApplication] delegate]managedObjectContext];
    
}

#pragma save and cancel
-(IBAction)save:(UIBarButtonItem *)sender {
    
    giftList.giftIdeas = textView.text;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)cancel:(UIBarButtonItem *)sender {
    
    [self.managedObjectContext rollback];
    [self dismissViewControllerAnimated:YES completion:nil];

    
}

@end
