//
//  Gift.m
//  gift
//
//  Created by Kyle Schwarzkopf on 6/8/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "Gift.h"
#import "Event.h"


@implementation Gift

@dynamic giftIdea;
@dynamic event;
@dynamic giftIdeas;

@end
