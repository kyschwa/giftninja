//
//  giftIdea.h
//  gift
//
//  Created by Kyle Schwarzkopf on 6/3/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "AddEventTableViewController.h"

@interface giftIdea : UITableViewController

@property (nonatomic, strong) Event *giftList;

@end
