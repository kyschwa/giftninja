//
//  Event.h
//  gift
//
//  Created by Kyle Schwarzkopf on 6/8/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Gift;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSDate * eventDate;
@property (nonatomic, retain) NSString * eventDateString;
@property (nonatomic, retain) NSString * eventName;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * reminderDate;
@property (nonatomic, retain) NSString * reminderDateString;
@property (nonatomic, retain) NSSet *gifts;
@property (nonatomic, retain) NSString *addGift2;

@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addGiftsObject:(Gift *)value;
- (void)removeGiftsObject:(Gift *)value;
- (void)addGifts:(NSSet *)values;
- (void)removeGifts:(NSSet *)values;

@end
