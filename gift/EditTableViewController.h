//
//  EditTableViewController.h
//  gift
//
//  Created by Kyle Schwarzkopf on 6/8/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "Gift.h"
#import "EventTableViewController.h"


@interface EditTableViewController : UITableViewController

@property (nonatomic, strong) Event *selectedEvent;

@end
