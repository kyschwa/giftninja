//
//  AddEventViewController.m
//  gift
//
//  Created by Kyle Schwarzkopf on 5/11/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "AddEventViewController.h"
#import "AddEventViewController.h"

#define kDatePickerIndex 1
#define kDatePickerCellHeight 164

@interface AddEventViewController ()

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (assign) BOOL datePickerIsShowing;
@property (weak, nonatomic) IBOutlet UITableViewCell *datePickerCell;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) NSDate *selectedBirthday;

@end

@implementation AddEventViewController
@synthesize addEvent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(UIBarButtonItem *)sender {
    
    [super cancelAndDismiss];
    
}

-(IBAction)save:(UIBarButtonItem *)sender {
    
    addEvent.name = _addName.text;
    addEvent.eventName = _addEventName.text;
    
    [super saveAndDismiss];
    
}

@end
