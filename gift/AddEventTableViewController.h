//
//  AddEventTableViewController.h
//  gift
//
//  Created by Kyle Schwarzkopf on 5/17/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreViewController.h"
#import "Event.h"

@interface AddEventTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong)Event *addEvent;

@property (weak, nonatomic) IBOutlet UITextField *addName;
@property (weak, nonatomic) IBOutlet UITextField *addEventName;

@property (weak, nonatomic) IBOutlet UILabel *eventTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *reminderTextLabel;

@property (weak, nonatomic) IBOutlet UITextView *textField;

- (IBAction)save:(UIBarButtonItem *)sender;
- (IBAction)cancel:(UIBarButtonItem *)sender;

- (IBAction)pickerDateChanged:(UIDatePicker *)sender;
- (IBAction)reminderDateChanged:(UIDatePicker *)sender;

@end
