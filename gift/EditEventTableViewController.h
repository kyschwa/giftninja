//
//  EditEventTableViewController.h
//  gift
//
//  Created by Kyle Schwarzkopf on 5/28/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "CoreViewController.h"
#import "editGift.h"

@interface EditEventTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) Event *editEvent;

@property (weak, nonatomic) IBOutlet UITextField *editName;
@property (weak, nonatomic) IBOutlet UITextField *editEventName;
@property (weak, nonatomic) IBOutlet UILabel *editDate;
@property (weak, nonatomic) IBOutlet UILabel *editReminder;
@property (weak, nonatomic) IBOutlet UITextView *textView;

-(IBAction)cancel:(UIBarButtonItem*)sender;
-(IBAction)editSave:(UIBarButtonItem*)sender;

-(IBAction)pickerDateChanged:(UIDatePicker*)sender;
-(IBAction)reminderDateChanged:(UIDatePicker*)sender;

@end
