//
//  EditEventViewController.m
//  gift
//
//  Created by Kyle Schwarzkopf on 5/10/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "EditEventViewController.h"

@interface EditEventViewController ()

@end

@implementation EditEventViewController
@synthesize editEvent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _editName.enabled = NO;
    _editEventName.enabled = NO;
    
    _editName.borderStyle = UITextBorderStyleNone;
    _editEventName.borderStyle = UITextBorderStyleNone;
    
    _editName.text = editEvent.name;
    _editEventName.text = editEvent.eventName;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)editSave:(UIBarButtonItem *)sender {
    
    if ([_editSaveButton.title isEqualToString:@"Edit"]) {
        NSLog(@"Edit button has been pressed");
        
        [self setTitle:@"Edit Event"];
        
        _editName.enabled = YES;
        _editEventName.enabled = YES;
        
        _editName.borderStyle = UITextBorderStyleRoundedRect;
        _editEventName.borderStyle = UITextBorderStyleRoundedRect;
        
        _editSaveButton.title = @"Save";
    }else if ([_editSaveButton.title isEqualToString:@"Save"]) {
        
        NSLog(@"Save Button Has Been Pressed");
        
        _editName.enabled = NO;
        _editEventName.enabled = NO;
        
        _editName.borderStyle = UITextBorderStyleNone;
        _editEventName.borderStyle = UITextBorderStyleNone;
        
        _editSaveButton.title = @"Edit";
        
        editEvent.name = _editName.text;
        editEvent.eventName = _editEventName.text;
        
        [super saveAndDismiss];
        
    }

}



- (IBAction)cancel:(UIBarButtonItem *)sender {
    [super cancelAndDismiss];
}
@end
