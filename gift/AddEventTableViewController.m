//
//  AddEventTableViewController.m
//  gift
//
//  Created by Kyle Schwarzkopf on 5/17/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import "AddEventTableViewController.h"
#import "AppDelegate.h"
#import "giftIdeaAdd.h"

#define kDatePickerIndex 1
#define kDatePickerCellHeight 164
#define kDatePickerIndex2 3
#define kDatePickerIndex3 5

static NSString *kDatePickerCellID = @"dateCell";
static NSString *kDatePickerCellID2 = @"reminderCell";
static NSString *kDatePickerCellID3 = @"textCell";

@interface AddEventTableViewController ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@property (assign) BOOL datePickerIsShowing;
@property (assign) BOOL datePickerIsShowing2;
@property (assign) BOOL TextFieldIsShowing;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSDateFormatter *dateFormatter2;
@property (strong, nonatomic) NSDate *selectedDate;
@property (strong, nonatomic) NSDate *selectedDate2;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UIDatePicker *reminderDatePicker;


@property (strong, nonatomic) UITextField *activeTextField;

@end

@implementation AddEventTableViewController
@synthesize addEvent;

-(NSManagedObjectContext *)managedObjectsContext {
    
    return [(AppDelegate*)[[UIApplication sharedApplication] delegate]managedObjectContext];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setUpEventDateLabel];
    
    [self setUpReminderDateLabel];
    
    [self signUpForKeyboardNotifications];
    
    [self hideDatePickerCell];
    
    [self hideReminderDatePickerCell];
    
    [self hideTextFieldCell];
    
    }
//    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
//    [self.tableView addGestureRecognizer:gestureRecognizer];


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSManagedObjectContext *)managedObjectContext {
    
    return [(AppDelegate *)[[UIApplication sharedApplication]delegate]managedObjectContext];
    
}

#pragma mark - Helper methods

- (void)setUpEventDateLabel {
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    NSDate *defaultDate = [NSDate date];
    
    self.eventTextLabel.text = [self.dateFormatter stringFromDate:defaultDate];
    self.eventTextLabel.textColor = [self.tableView tintColor];
    
    self.selectedDate = defaultDate;
}

- (void)setUpReminderDateLabel {
    
    NSDate *reminderDate = [NSDate date];
    
    self.dateFormatter2 = [[NSDateFormatter alloc] init];
    [self.dateFormatter2 setDateStyle:NSDateFormatterMediumStyle];
    [self.dateFormatter2 setTimeStyle:NSDateFormatterNoStyle];
    
    self.reminderTextLabel.text = [self.dateFormatter2 stringFromDate:reminderDate];
    self.reminderTextLabel.textColor = [self.tableView tintColor];
    
    self.selectedDate2 = reminderDate;
    
}

- (void)signUpForKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow) name:UIKeyboardWillShowNotification object:nil];
    
}

- (void)keyboardWillShow {
    
    if (self.datePickerIsShowing) {
        
        [self hideDatePickerCell];
        
    }
    
    if (self.datePickerIsShowing2) {
        
        [self hideReminderDatePickerCell];
        
    }
    
}

#pragma mark - Table view methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = self.tableView.rowHeight;
    
    if (indexPath.section == 1) {
    
    if (indexPath.row == kDatePickerIndex){
        
        height = self.datePickerIsShowing ? kDatePickerCellHeight : 0.0f;
        
    } if (indexPath.row == kDatePickerIndex2) {
        
        height = self.datePickerIsShowing2 ? kDatePickerCellHeight: 0.0f;
        }
    
    } if (indexPath.row == kDatePickerIndex3) {
       
        height = self.TextFieldIsShowing ? kDatePickerCellHeight : 0.0f;
        
    }
    return height;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section == 1) {
    
    if (indexPath.row == 0){
        
        if (self.datePickerIsShowing){
            
            [self hideDatePickerCell];
            
        }else {
            
            [self.activeTextField resignFirstResponder];
            
            [self showDatePickerCell];
        }
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    //reminder

            if (indexPath.row == 2) {
                if (self.datePickerIsShowing2) {
                    [self hideReminderDatePickerCell];
                }
                else {
                    
                [self.activeTextField resignFirstResponder];
                
                [self showReminderDatePickerCell];
            }
    }
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
             
        if (indexPath.row == 4) {
                 
                 if (self.TextFieldIsShowing) {
                     
                     [self hideTextFieldCell];
                     
                 } else {
                     
                     [self.activeTextField resignFirstResponder];
                     
                     [self showTextFieldCell];
                 }
                 
             }
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}


- (void)showDatePickerCell {
    
    self.datePickerIsShowing = YES;
    
    [self.tableView beginUpdates];
    
    [self.tableView endUpdates];
    
    
    
    self.datePicker.hidden = NO;
    self.datePicker.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.datePicker.alpha = 1.0f;
        
    }];
}

- (void)hideDatePickerCell {
    
    self.datePickerIsShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.datePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.datePicker.hidden = YES;
                     }];
}


- (void)showTextFieldCell {
    
    self.TextFieldIsShowing = YES;
    
    [self.tableView beginUpdates];
    
    [self.tableView endUpdates];
    
    
    
    self.textField.hidden = NO;
    self.textField.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.textField.alpha = 1.0f;
        
    }];
}

- (void)hideTextFieldCell {
    
    self.TextFieldIsShowing = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.textField.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.textField.hidden = YES;
                     }];
}

#pragma mark - UITextFieldDelegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.activeTextField = textField;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 6;

        default:
            return section;
            break;
    }
}

#pragma reminder date cell


- (void)showReminderDatePickerCell {
    
    self.datePickerIsShowing2 = YES;
    
    [self.tableView beginUpdates];
    
    [self.tableView endUpdates];
    
    self.reminderDatePicker.hidden = NO;
    
    self.reminderDatePicker.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        self.reminderDatePicker.alpha = 1.0f;
        
    }];
    
}

- (void)hideReminderDatePickerCell {
    
    self.datePickerIsShowing2 = NO;
    
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.reminderDatePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.reminderDatePicker.hidden = YES;
                     }];
}




#pragma  IBActions

- (IBAction)save:(UIBarButtonItem *)sender {
    
    addEvent.name = _addName.text;
    addEvent.eventName = _addEventName.text;
    addEvent.reminderDateString = _reminderTextLabel.text;
    addEvent.eventDateString = _eventTextLabel.text;
    addEvent.addGift2 = _textField.text;
    
    NSError *error = nil;
    if ([self.managedObjectContext hasChanges]) {
        if (![self.managedObjectContext save:&error]) { //save failed
            NSLog(@"Save Failed: %@", [error localizedDescription]);
        } else {
            NSLog(@"Save Succeeded");
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //local Notification
    //event date
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = _selectedDate;
    localNotification.alertBody = [NSString stringWithFormat:@"%@ %@", _addName, _addEventName];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    //reminder event date
    localNotification.fireDate = _selectedDate2;
    localNotification.alertBody = [NSString stringWithFormat:@"%@ %@", _addName, _addEventName];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];

    
}


- (IBAction)cancel:(UIBarButtonItem *)sender {
    
    [self.managedObjectContext rollback];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)pickerDateChanged:(UIDatePicker *)sender {
    
    self.eventTextLabel.text = [self.dateFormatter stringFromDate:sender.date];
    
}

- (IBAction)reminderDateChanged:(UIDatePicker *)sender {
    self.reminderTextLabel.text = [self.dateFormatter2 stringFromDate:sender.date];
    
}

#pragma keyboard dismiss

//-(void)hideKeyboard{
//    
//    [_addName resignFirstResponder];
//    [_addEventName resignFirstResponder];
//
//    
//}
//
//-(void)dismissKeyboard {
//    
//    
//    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
//    [self.tableView addGestureRecognizer:gestureRecognizer];
//    
//}

- (IBAction)textFieldFinished:(id)sender {
    
    [sender resignFirstResponder];

}


@end
