//
//  CoreViewController.h
//  gift
//
//  Created by Kyle Schwarzkopf on 5/10/14.
//  Copyright (c) 2014 Kyle Schwarzkopf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoreViewController : UIViewController

-(void)cancelAndDismiss;

-(void)saveAndDismiss;

@end
